document.querySelector('#txt-first-name');

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener('keyup', keyEvent);
txtLastName.addEventListener('keyup', keyEvent);

function keyEvent () {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
};