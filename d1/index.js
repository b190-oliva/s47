

// document.getElementById('txt-first-name');

document.querySelector('#txt-first-name');

const txtFirstName = document.querySelector('#txt-first-name');

const spanFullName = document.querySelector('#span-full-name');

// addEvenListener takes two arguments, first is a string that identifies the event to which the code will react
// second argument is a function that the listener will execute

//keypress - realtime logger for key event
txtFirstName.addEventListener('keyup', (event) =>{
	// innerHTML - this allows the element to record/duplicate the value of the selected variable(txtFirstName.value)
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event) =>{
	console.log(event.target);
	console.log(event.target.value);
});